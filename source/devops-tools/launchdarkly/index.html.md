---
layout: markdown_page
title: "LaunchDarkly"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

## Summary
   - minimal requirement <-- comment. delete this line
## Strengths
## Weaknesses
## Who buys and why
## Comments/Anecdotes
   - possible customer issues with product  <-- comment. delete this line
   - sample benefits and success stories  <-- comment. delete this line
   - date, source, insight  <-- comment. delete this line
## Resources
   - links to communities, etc  <-- comment. delete this line
   - bulleted list  <-- comment. delete this line
## FAQs
 - about the product  <-- comment. delete this line
## Integrations
## Pricing
   - summary, links to tool website  <-- comment. delete this line
### Value/ROI
   - link to ROI calc?  <-- comment. delete this line
## Questions to ask
   - positioning questions, traps, etc.  <-- comment. delete this line
## Comparison
   - features comparison table will follow this <-- comment. delete this line

<!------------------Begin page additions below this line ------------------ -->

## On this page
{:.no_toc}

- TOC
{:toc}

## Summary
Launchdarkly is a feature delivery and feature management service offering.  This means that the service they provide allows their customers to dynamically control when new features (within their application) become available to their end users.  Launchdarkly’s service offering represents a tool within the DevOps Toolchain that aligns within the Continuous Delivery (CD) tool category.  


## Strengths
Launchdarkly provides a very sophisticated feature management service.  Their service allows developers to hide features, turn on features for targeted groups of users or execute a kill switch on a feature - to name a few.  With their offering, Development and Operations teams can deliver code faster with minimal risk by separating code deployments from feature deliveries.

## Gaps
Launchdarkly is a niche solution that must be overlaid within a customer's existing DevOps Toolchain.  Though they do provide more comprehensive feature flag configuration capabilities, they fail to meet customers complete DevOps needs with a single application toolchain.  Additionally, Launchdarkly’s price point per user (starting at $90/user for their lower tier plan and $390/user for their mid-tier plan) seems costly for a single tool that is missing key DevOps features and capabilities such as [Source Code Management (SCM)] (https://about.gitlab.com/stages-devops-lifecycle/source-code-management/), [Continuous Integration (CI)] (https://about.gitlab.com/stages-devops-lifecycle/continuous-integration/) and [Security Embedded within the DevOps workflow (DevSecOps)](https://about.gitlab.com/solutions/dev-sec-ops/).

## Resources
[Launchdarkly.com]
(https://launchdarkly.com/)

[Launchdarkly Pricing]
(https://launchdarkly.com/pricing/)

[Launchdarkly Wikipedia]
(https://en.wikipedia.org/wiki/Feature_toggle)

[GitLab Feature Flags and Competitive Landscape] 
(https://about.gitlab.com/direction/release/feature_flags/)


## Comparison
